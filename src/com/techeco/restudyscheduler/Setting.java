/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techeco.restudyscheduler;

/**
 *
 * @author truongnln
 */
public class Setting {

    private int maxTypeOfSlot;
    private int minNoStudent;

    public Setting() {
    }

    public Setting(int maxTypeOfSlot, int minNoStudent) {
        this.maxTypeOfSlot = maxTypeOfSlot;
        this.minNoStudent = minNoStudent;
    }

    public int getMaxTypeOfSlot() {
        return maxTypeOfSlot;
    }

    public void setMaxTypeOfSlot(int maxTypeOfSlot) {
        this.maxTypeOfSlot = maxTypeOfSlot;
    }

    public int getMinNoStudent() {
        return minNoStudent;
    }

    public void setMinNoStudent(int minNoStudent) {
        this.minNoStudent = minNoStudent;
    }
}
