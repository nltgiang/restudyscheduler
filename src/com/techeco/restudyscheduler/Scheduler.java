/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techeco.restudyscheduler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author truongnln
 */
public class Scheduler {

    private Dependence dependence;
    private List<List<String>> noProcessList;
    private List<String> conflictSubjects;

    public static void main(String[] args) {
        List<RegistrationData> registrations = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("registrations.data"))) {
            String line;
            while ((line = br.readLine()) != null) {
                Pattern p = Pattern.compile("^([^#]+) (\\S+) (\\S+)$");
                Matcher m = p.matcher(line);
                if (m.find()) {
                    RegistrationData data = new RegistrationData();
                    data.setRollNumber(m.group(1));
                    data.setSubjectCode(m.group(2));
                    data.setExpectedShift(m.group(3));
                    registrations.add(data);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (!registrations.isEmpty()) {
            Scheduler scheduler = new Scheduler();
            Setting setting = new Setting(4, 1);
            scheduler.run(setting, registrations);
            for (String conflictSubject : scheduler.getConflictSubjects()) {
                System.out.println(conflictSubject);
            }
        }
    }

    public List<RegistrationData> getRejectedByClassLimit() {
        return this.dependence.getRemovedRegistrations();
    }

    public List<List<String>> getNoProcessList() {
        return noProcessList;
    }

    public List<String> getConflictSubjects() {
        return conflictSubjects;
    }

    private boolean checkFullConnected(HashSet<String> connectedPart) {
        for (String node : connectedPart) {
            for (String nbNode : connectedPart) {
                if (!node.equals(nbNode)) {
                    if (this.dependence
                            .getRegistration(node, nbNode)
                            .isEmpty()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void dfs(String uEdge, List<String> nodes, List<String> component) {
        if (nodes.contains(uEdge)) {
            component.add(uEdge);
            nodes.remove(uEdge);
            int i = 0;
            while (nodes.size() > 0 && i < nodes.size()) {
                String vEdge = nodes.get(i);
                if (this.dependence.getRegistration(uEdge, vEdge).size() > 0) {
                    this.dfs(vEdge, nodes, component);
                } else {
                    i++;
                }
            }
        }
    }

    public void run(Setting setting, List<RegistrationData> registrations) {
        this.dependence = new Dependence();
        List<List<String>> components = new ArrayList<>();

        //initial data with parameter
        this.dependence.addAll(registrations);
        this.dependence.removeRegistration(setting.getMinNoStudent());
        Set<String> subjects = this.dependence.getSubjects();
        //seperate subjects to connected-graphs
        List<String> listSubject = new ArrayList<>(subjects);
        while (listSubject.size() > 0) {
            List<String> component = new ArrayList<>();
            String subject = listSubject.get(0);
            this.dfs(subject, listSubject, component);
            components.add(component);
        }
        //get no-process graph
        this.noProcessList = components.stream()
                .filter(a -> a.size() <= setting.getMaxTypeOfSlot())
                .collect(Collectors.toList());
        //get process graph
        List<List<String>> processList = components.stream()
                .filter(a -> a.size() > setting.getMaxTypeOfSlot())
                .collect(Collectors.toList());
        this.conflictSubjects = new ArrayList<>();
        //find conflict subject pairs
        processList.forEach((item) -> {
            boolean hasConflict = this.processComponent(
                    item,
                    this.conflictSubjects,
                    setting.getMaxTypeOfSlot());
            if (!hasConflict) {
                noProcessList.add(item);
            }
        });
    }

    private boolean processComponent(
            List<String> component,
            List<String> conflictSubjects,
            int maxTypeOfSlot) {
        HashSet<String> tracking = new HashSet<>();
        boolean hasConflict = false;
        for (String subject : component) {
            tracking.add(subject);
            List<String> nearBySubjects
                    = this.dependence.getNearBySubjects(subject);
            nearBySubjects.removeAll(tracking);
            if (nearBySubjects.size() >= maxTypeOfSlot - 1) {
                //get subject full-connected-parts (FCParts)
                List<HashSet<String>> subjectFCParts = new ArrayList<>();
                List<ConflictScheduling> conflicts
                        = new ArrayList<>();
                this.findFullConnected(
                        subject,
                        nearBySubjects,
                        subjectFCParts,
                        maxTypeOfSlot,
                        null,
                        0);
                //get near by full-connected-parts (FCParts)
                hasConflict = hasConflict || this.preparePrediction(subject,
                        subjectFCParts, nearBySubjects, conflicts);
                this.findConflictSubject(conflicts, conflictSubjects);
            }
        }
        return hasConflict;
    }

    private void findConflictSubject(
            List<ConflictScheduling> conflicts,
            List<String> conflictSubjects) {
        for (ConflictScheduling conflict : conflicts) {
            conflict.getPredictConflicted().stream()
                    .filter((prediction) -> (this.dependence.getRegistration(conflict.getSubject(), prediction).size() > 0))
                    .forEachOrdered((prediction) -> {
                        String key1 = String.format(
                                "%1$s_%2$s",
                                conflict.getSubject(),
                                prediction);
                        String key2 = String.format(
                                "%2$s_%1$s",
                                conflict.getSubject(),
                                prediction);
                        if (!conflictSubjects.contains(key1)
                                && !conflictSubjects.contains(key2)) {
                            conflictSubjects.add(key1);
                        }
                    });
        };
    }

    private void findFullConnected(
            String subject,
            List<String> nearBySubjects,
            List<HashSet<String>> fullConnectedPart,
            int maxTypeOfSlot,
            HashSet<String> connectedPart,
            int browseIndex) {
        if (connectedPart == null) {
            connectedPart = new HashSet<>();
            connectedPart.add(subject);
        }
        //break condition
        if (connectedPart.size() >= maxTypeOfSlot) {
            boolean isFullConnected = this.checkFullConnected(connectedPart);
            if (isFullConnected) {
                fullConnectedPart.add(new HashSet<>(connectedPart));
            }
            return;
        }
        //generate non-loop nodes for conected-part
        int browseLimit = browseIndex
                + (nearBySubjects.size()
                - (maxTypeOfSlot - connectedPart.size())
                - browseIndex);
        for (int i = browseIndex; i <= browseLimit; i++) {
            String browseSubject = nearBySubjects.get(i);
            connectedPart.add(browseSubject);
            this.findFullConnected(
                    subject,
                    nearBySubjects,
                    fullConnectedPart,
                    maxTypeOfSlot,
                    connectedPart,
                    i + 1);
            connectedPart.remove(browseSubject);
        }
    }

    private boolean preparePrediction(
            String subject,
            List<HashSet<String>> subjectFCParts,
            List<String> nearBySubjects,
            List<ConflictScheduling> conflicts) {
        boolean hasConflict = false;
        //browse in full-connected-parts of subject
        for (HashSet<String> subjectFCP : subjectFCParts) {
            ConflictScheduling conflict = new ConflictScheduling(subject);
            conflict.addAllSubjectFcp(subjectFCP);
            HashSet<String> checkingNodes = subjectFCP
                    .stream()
                    .filter(a -> !a.equals(subject))
                    .collect(Collectors.toCollection(HashSet::new));
            List<String> noContainNbs = nearBySubjects
                    .stream()
                    .filter(a -> !subjectFCP.contains(a))
                    .collect(Collectors.toList());
            //find connected-part from near-by subjects and current parts
            for (String item : noContainNbs) {
                checkingNodes.add(item);
                //check is full-connected
                boolean fcResult = this.checkFullConnected(checkingNodes);
                if (fcResult) {
                    conflict.getPredictConflicted().add(item);
                }
                checkingNodes.remove(item);
            }
            if (conflict.getPredictConflicted().size() > 0) {
                conflicts.add(conflict);
                hasConflict = true;
            }
        }
        return hasConflict;
    }

}
