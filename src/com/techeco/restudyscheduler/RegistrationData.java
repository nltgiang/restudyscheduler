/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techeco.restudyscheduler;

/**
 *
 * @author truongnln
 */
public class RegistrationData {
    private String rollNumber;
    private String subjectCode;
    private String expectedShift;
    private int errorCode;

    public RegistrationData() {
    }
    
    public RegistrationData(String rollNumber, String subjectCode, String expectedShift) {
        this.rollNumber = rollNumber;
        this.subjectCode = subjectCode;
        this.expectedShift = expectedShift;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getExpectedShift() {
        return expectedShift;
    }

    public void setExpectedShift(String expectedShift) {
        this.expectedShift = expectedShift;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }    
}
