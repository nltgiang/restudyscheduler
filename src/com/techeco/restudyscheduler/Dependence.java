/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techeco.restudyscheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author truongnln
 */
public class Dependence {

    private List<RegistrationData> registrations;
    private List<RegistrationData> removedRegistrations;
    private HashMap<String, List<RegistrationData>> subjectMap;
    private HashMap<String, HashSet<String>> studentMap;

    public Dependence() {
        this.registrations = new ArrayList<>();
        this.removedRegistrations = new ArrayList<>();
        this.subjectMap = new HashMap<>();
        this.studentMap = new HashMap<>();
    }

    public List<RegistrationData> getRemovedRegistrations() {
        return removedRegistrations;
    }
    
    public void add(RegistrationData data) {
        this.registrations.add(data);
        if (this.subjectMap.containsKey(data.getSubjectCode())) {
            this.subjectMap
                    .get(data.getSubjectCode())
                    .add(data);
        } else {
            List<RegistrationData> list = new ArrayList<>();
            list.add(data);
            this.subjectMap.put(data.getSubjectCode(), list);
        }
        if (this.studentMap.containsKey(data.getRollNumber())) {
            this.studentMap
                    .get(data.getRollNumber())
                    .add(data.getSubjectCode());
        } else {
            HashSet<String> list = new HashSet<>();
            list.add(data.getSubjectCode());
            this.studentMap.put(data.getRollNumber(), list);
        }
    }

    public void addAll(List<RegistrationData> data) {
        for (RegistrationData registrationData : data) {
            this.add(registrationData);
        }
    }

    public Set<String> getSubjects() {
        return this.subjectMap.keySet();
    }

    public List<RegistrationData> getSubjectRegistration(String subjectCode) {
        return this.subjectMap.get(subjectCode);
    }

    public List<String> getNearBySubjects(String subjectCode) {
        List<String> result = new ArrayList<>();
        for (Map.Entry<String, HashSet<String>> entry : studentMap.entrySet()) {
            HashSet<String> value = entry.getValue();
            if (value.contains(subjectCode)) {
                for (String subCode : value) {
                    if (!subCode.equals(subjectCode)
                            && !result.contains(subCode)) {
                        result.add(subCode);
                    }
                }
            }
        }
        return result;
    }

    public HashSet<String> getRegistration(String fSubject, String sSubject) {
        HashSet<String> result = new HashSet<>();
        for (Map.Entry<String, HashSet<String>> entry : studentMap.entrySet()) {
            HashSet<String> value = entry.getValue();
            if (value.contains(fSubject) && value.contains(sSubject)) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    public void removeRegistration(int minStudent) {
        for (Map.Entry<String, List<RegistrationData>> entry : subjectMap.entrySet()) {
            String key = entry.getKey();
            List<RegistrationData> value = entry.getValue();
            if (value.size() < minStudent) {
                for (RegistrationData registrationData : value) {
                    this.registrations.remove(registrationData);
                    this.removedRegistrations.add(registrationData);
                    this.studentMap
                            .get(registrationData.getRollNumber())
                            .remove(registrationData.getSubjectCode());
                }
                value.clear();
            }
        }
    }
}
