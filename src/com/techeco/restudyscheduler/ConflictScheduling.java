/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techeco.restudyscheduler;

import java.util.HashSet;

/**
 *
 * @author truongnln
 */
public class ConflictScheduling {

    private String subject;
    private HashSet<String> subjectFcp;
    private HashSet<String> predictConflicted;

    public ConflictScheduling(String subject) {
        this.subject = subject;
        this.subjectFcp = new HashSet<>();
        this.predictConflicted = new HashSet<>();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public HashSet<String> getSubjectFcp() {
        return subjectFcp;
    }

    public void setSubjectFcp(HashSet<String> subjectFcp) {
        this.subjectFcp = subjectFcp;
    }

    public void addAllSubjectFcp(HashSet<String> subjectFcp) {
        this.subjectFcp.addAll(subjectFcp);
    }

    public HashSet<String> getPredictConflicted() {
        return predictConflicted;
    }

    public void setPredictConflicted(HashSet<String> predictConflicted) {
        this.predictConflicted = predictConflicted;
    }

    public void addAllPredictConflicted(HashSet<String> predictConflicted) {
        this.predictConflicted.addAll(predictConflicted);
    }
}
